import React from "react";
import {QueryClientProvider, QueryClient} from "react-query";
import Header from "./components/header/Header";
import Sheet from "./components/sheet/Sheet";
import "./App.css";

const queryClient =new QueryClient();

const App = () => {

  return <QueryClientProvider client={queryClient}>
            <div className="App">
              <Header/>
              <Sheet/>
            </div>
         </QueryClientProvider>
};

export default App;
