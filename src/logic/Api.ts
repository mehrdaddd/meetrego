import axios from "axios";

//today Date format
const today = new Date();
const dd = String(today.getDate()).padStart(2, '0');
const mm = String(today.getMonth() + 1).padStart(2, '0');
const yyyy = today.getFullYear();
const todayFormatDate = yyyy + '-' + mm + '-' + dd;
let saveKey: any = "today"

// #todo define types

// api Nasa get request for today
const titles = [{ value: "Asteroid name" }, { value: "Time" }, { value: "Velocity (K/H)" }, { value: "Miss distance (K)" }, { value: "Estimated diameter (max, K)" }, { value: "Potential Hazard" }, { value: "Id" }, { value: "Notes" }];

const reformatOption = (item: any) => [{ value: item.name }, { value: item.close_approach_data[0].close_approach_date_full }, { value: item.close_approach_data[0].relative_velocity.kilometers_per_hour }, { value: item.close_approach_data[0].miss_distance.kilometers }, { value: item.estimated_diameter.kilometers.estimated_diameter_max }, { value: item.is_potentially_hazardous_asteroid }, { value: item.id }, { value: "" }]

export const todayQuery = async () => {
    const res: any = await axios.get("https://neowsapp.com/rest/v1/feed/today?detailed=false");
    const jsonData = await res.data;
    const reformat: Array<any> = [];
    jsonData.near_earth_objects[todayFormatDate].forEach((item: any) => reformat.push(reformatOption(item)));
    return [
        titles,
    ].concat(reformat);
}

// api Nasa get request in date range
export const DateQuery = async ({ queryKey }: any) => {
    const [key, start, end] = queryKey;
    const res: any = await axios.get(`https://neowsapp.com/rest/v1/feed?start_date=${start}&end_date=${end}&detailed=false`);
    const jsonData = await res.data;
    const reformat: Array<any> = [];
    for (const dateArray in jsonData.near_earth_objects) {
        jsonData.near_earth_objects[dateArray].forEach((item: any) => reformat.push(reformatOption(item)));
    }
    return [titles,
    ].concat(reformat);
}