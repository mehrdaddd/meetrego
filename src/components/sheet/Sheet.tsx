import React, { useState, useEffect } from "react";
import { useQuery } from "react-query";
import Spreadsheet from "react-spreadsheet";
import { todayQuery, DateQuery } from "../../logic/Api";
import "./sheet.css";

const Sheet = () => {
  const [start, setStart] = useState<string>("");
  const [end, setEnd] = useState<string>("");
  const [page, setPage] = useState<number>(1);
  const [pageNumbers, setPageNumbers] = useState<number>(1);
  const [paginationData, setPaginationData] = useState();
  const [showSheet, setShowSheet] = useState<string>("today");

  const { refetch, data }: any = useQuery('nasa-data-today', todayQuery, {
    select: (data) => {
      data.map(item => {
        if (localStorage[item[6].value]) {
          item[7] = { value: localStorage[item[6].value] }
        }
      })
      return data;
    },
  });

  const { refetch: dateFilter, data: dateData } = useQuery(['nasa-data-date', start, end], DateQuery, {
    enabled: false,
    select: (data) => {
      data.map(item => {
        if (localStorage[item[6].value]) {
          item[7] = { value: localStorage[item[6].value] }
        }
      })
      return data;
    },
  });

  useEffect(() => {
    if (dateData) {
      let cutData: any = dateData.slice((page - 1) * 10 + 1, 10 * page);
      var paginationData: any = [dateData[0]].concat(cutData);
      setPaginationData(paginationData);
      setPageNumbers(Math.round(dateData.length / 10));
    }
  }, [dateData, page])

  const addNode = (e: any) => {
    e.map((item: any) => {
      localStorage.removeItem(item[6].value);
      item[6].value !== "Id" && item[7].value && localStorage.setItem(item[6].value, item[7].value);
    });
  }

  return (
    <>
      <div className="filter-wrapper">
        <div className="button" onClick={() => {
          setShowSheet("today"); setPaginationData(undefined);
          setPageNumbers(1); refetch
        }}>Today</div>
        <div className="filter-date">
          <input type="date" id="start" name="start" onChange={e => setStart(e.target.value)} />
          to
          <input type="date" id="end" name="end" onChange={e => setEnd(e.target.value)} />
          <div className="button" onClick={() => { setShowSheet("dateData"); dateFilter() }}>Filter</div>
        </div>
      </div>
      {showSheet === "today" ? (data && <Spreadsheet data={data} onChange={e => { addNode(e) }} />) :
        (paginationData && <Spreadsheet data={paginationData} onChange={e => { addNode(e) }} />)}
      {paginationData && <div className="pagination">
        <div onClick={() => page > 2 && setPage(page - 1)} >&laquo;</div>
        {Array.from(Array(pageNumbers), (e, i) => {
          return <div className={page === (i + 1) ? "active" : ""} key={i + 1} onClick={() => setPage(i + 1)} >{i + 1}</div>
        })}
        <div onClick={() => page < pageNumbers && setPage(page + 1)}>&raquo;</div>
      </div>}
    </>
  )
}

export default Sheet;
